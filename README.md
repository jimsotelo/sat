## Sentiment Analysis using Twitter Data

**Last Change:** December 2013

**Author:** Jos� Manuel

---
### Information

The goal of this paper is to analyse the tone of the social media in different states.

---
### To do
 * Check reproducibility.
 * Design robustness tests.

### Done

 * Basic files added.
 * Python script to download geotagged tweets.
 * R script to classify every tweet according to its state.
 * R script to plot the results.
 * Python script to translate spanish words using the Google Translate API.
 * Python script to evaluate the sentiment of spanish tweets.
 * Integrate code with the latex document (done using Make).
 * Improve format of plots.
 * Write text of document.